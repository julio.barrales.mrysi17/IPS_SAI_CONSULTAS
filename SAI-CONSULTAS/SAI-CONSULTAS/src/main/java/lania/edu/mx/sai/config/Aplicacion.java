package lania.edu.mx.sai.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 *
 * @author jaguilar
 */
public class Aplicacion implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext contenedor) throws ServletException {
        WebApplicationContext contextoSpring = getContext();
        ServletRegistration.Dynamic servlet = contenedor.addServlet("despachador", new DispatcherServlet(contextoSpring));
        servlet.setLoadOnStartup(2);
        servlet.addMapping("/");
    }

    private WebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext contexto = new AnnotationConfigWebApplicationContext();
        String nombreClase = this.getClass().getCanonicalName();
        String paqueteClase = nombreClase.substring(0, nombreClase.lastIndexOf('.'));
        contexto.setConfigLocation(paqueteClase);
        return contexto;
    }
}

