/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lania.edu.mx.sai.entidades;

/**
 *
 * @author jcesa
 */
public class Consultas {
    private Integer idConsulta;
    private String nombreConsulta;

    public Integer getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(Integer idConsulta) {
        this.idConsulta = idConsulta;
    }

    public String getNombreConsulta() {
        return nombreConsulta;
    }

    public void setNombreConsulta(String nombreConsulta) {
        this.nombreConsulta = nombreConsulta;
    }

    public Consultas(Integer idConsulta, String nombreConsulta) {
        this.idConsulta = idConsulta;
        this.nombreConsulta = nombreConsulta;
    }
    
}
