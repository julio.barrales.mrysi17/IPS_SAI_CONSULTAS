/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pruebaunidad.PruebaUnidad;

/**
 *
 * @author jcesa
 */
public class PruebaUnidadTest {
    
    public PruebaUnidadTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    /*
    @Test
    public void testSinComision() {
        System.out.println("comision");
        BigDecimal meta = new BigDecimal("10000");
        BigDecimal[] ventas = new BigDecimal[]{
            new BigDecimal("12000"),
            new BigDecimal("19000"),
            BigDecimal.ZERO
        };
        PruebaUnidad instance = new PruebaUnidad();
        BigDecimal expResult = BigDecimal.ZERO;
        BigDecimal result = instance.comision(meta, ventas);
        assertEquals(expResult,result);
    }
    
    @Test
    public void testConComision() {
        System.out.println("comision");
        BigDecimal meta = new BigDecimal("10000");
        BigDecimal[] ventas = new BigDecimal[]{
            new BigDecimal("12000"),
            new BigDecimal("19000"),
            BigDecimal.ZERO
        };
        PruebaUnidad instance = new PruebaUnidad();
        BigDecimal expResult = BigDecimal.ZERO;
        BigDecimal result = instance.comision(meta, ventas);
        assertEquals(expResult,result);
    }
    */
    @Test
    public void testArreglosCeroArreglos() {
        System.out.println("TotalPersonas,TiempoArreglo");
        int TotalArreglos = 0;
        
        PruebaUnidad instance = new PruebaUnidad();
        int expResult = 0;
        int [] result = instance.calcularPersonasTiempoParaArreglos(TotalArreglos);
        assertEquals(expResult,result[0]);
    }
    
    @Test
    public void testArreglosUnArreglo() {
        System.out.println("TotalPersonas,TiempoArreglo");
        int TotalArreglos = 1;
        
        PruebaUnidad instance = new PruebaUnidad();
        int expResult = 1;
        int [] result = instance.calcularPersonasTiempoParaArreglos(TotalArreglos);
        assertEquals(expResult,result[0]);
    }
    
    @Test
    public void testArreglos26Arreglos() {
        System.out.println("TotalPersonas,TiempoArreglo");
        int TotalArreglos = 26;
        
        PruebaUnidad instance = new PruebaUnidad();
        int expResult = 2;
        int [] result = instance.calcularPersonasTiempoParaArreglos(TotalArreglos);
        assertEquals(expResult,result[0]);
    }

    @Test
    public void testArreglosMas50() {
        System.out.println("TotalPersonas,TiempoArreglo");
        int TotalArreglos = 60;
        
        PruebaUnidad instance = new PruebaUnidad();
        int expResult = 3;
        int [] result = instance.calcularPersonasTiempoParaArreglos(TotalArreglos);
        assertEquals(expResult,result[0]);
    }

    @Test
    public void testArreglosmas75() {
        System.out.println("TotalPersonas,TiempoArreglo");
        int TotalArreglos = 75;
        
        PruebaUnidad instance = new PruebaUnidad();
        int expResult = 4;
        int [] result = instance.calcularPersonasTiempoParaArreglos(TotalArreglos);
        assertEquals(expResult,result[0]);
    }
    
    @Test
    public void testArreglosTiempo() {
        System.out.println("TotalPersonas,TiempoArreglo");
        int TotalArreglos = 75;
        
        PruebaUnidad instance = new PruebaUnidad();
        int expResult = 281;
        int [] result = instance.calcularPersonasTiempoParaArreglos(TotalArreglos);
        assertEquals(expResult,result[1]);
    }
}
